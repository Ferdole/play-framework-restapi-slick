## play framework restAPI with slick

Simple restAPI that does CRUD over a student DB in PostgreSQL.

### Descrrption

Some files were used just to learn how this framework works. Below is a short description of those used for the restAPI implmentation.

- `app.codeGen.CodeGen` is used to generate the table schema for the tables in student DB 

- `app.controllers.dbController` is used for the student implementation 
    - all the other controllers are there just for learning purposes
    
- `app.service.studentDatabaseService` is used to execute queries on the table

- `app.conf.routes` routes used

- `app.conf.appplication.conf` slick database configuration

### Run it

- you will need an instance of Postgresql. (will upload a full script that builds a docker containter with all the needed stuff)
