package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.PostgresProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Stud.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Stud
   *  @param name Database column name SqlType(varchar)
   *  @param age Database column age SqlType(int4)
   *  @param email Database column email SqlType(varchar)
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey */
  case class StudRow(name: String, age: Int, email: String, id: Int)
  /** GetResult implicit for fetching StudRow objects using plain SQL queries */
  implicit def GetResultStudRow(implicit e0: GR[String], e1: GR[Int]): GR[StudRow] = GR{
    prs => import prs._
    StudRow.tupled((<<[String], <<[Int], <<[String], <<[Int]))
  }
  /** Table description of table stud. Objects of this class serve as prototypes for rows in queries. */
  class Stud(_tableTag: Tag) extends profile.api.Table[StudRow](_tableTag, "stud") {
    def * = (name, age, email, id) <> (StudRow.tupled, StudRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(name), Rep.Some(age), Rep.Some(email), Rep.Some(id))).shaped.<>({r=>import r._; _1.map(_=> StudRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column name SqlType(varchar) */
    val name: Rep[String] = column[String]("name")
    /** Database column age SqlType(int4) */
    val age: Rep[Int] = column[Int]("age")
    /** Database column email SqlType(varchar) */
    val email: Rep[String] = column[String]("email")
    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
  }
  /** Collection-like TableQuery object for table Stud */
  lazy val Stud = new TableQuery(tag => new Stud(tag))
}
