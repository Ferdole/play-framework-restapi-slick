package models

import play.api.libs.json.{JsError, JsObject, JsResult, JsSuccess, JsValue, Json, Reads, Writes}


case class StudentNoId(name: String, age: Int, email: String){

  override def toString: String = "name: %s / age: %d / email: %s ".format(name, age, email)


}

object StudentNoId {

  implicit val studentWrite: Writes[StudentNoId] = Json.writes[StudentNoId]
  val studentReads: Reads[StudentNoId] = Json.reads[StudentNoId]

  // user to validate if body has more keys than necessary
  implicit val strictReads: Reads[StudentNoId] = new Reads[StudentNoId] {
    val expectedKeys = Set("name", "age", "email")
    def reads(jsv: JsValue): JsResult[StudentNoId] = {
      StudentNoId.studentReads.reads(jsv).flatMap { student =>
        checkUnwantedKeys(jsv, student)
      }
    }
    private def checkUnwantedKeys(jsv: JsValue, student: StudentNoId): JsResult[StudentNoId] = {
      val obj = jsv.asInstanceOf[JsObject]
      val keys = obj.keys
      val unwanted = keys.diff(expectedKeys)
      println(s"Object: ${obj} // Keys: ${keys} // unwanted: ${unwanted}")
      if (unwanted.isEmpty) {
        JsSuccess(student)
      } else {
        JsError("Keys: %s are invalid for Student object".format(unwanted.mkString(",")))
      }

    }

  }


}


case class Student(id: Int ,name: String, age: Int, email: String)

object Student {

  implicit val studentWrite: Writes[Student] = Json.writes[Student]
  implicit val studentReads: Reads[Student] = Json.reads[Student]
}
