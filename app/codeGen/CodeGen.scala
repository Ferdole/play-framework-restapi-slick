package codeGen

object CodeGen extends App {
  slick.codegen.SourceCodeGenerator.run(
    "slick.jdbc.PostgresProfile",
    "org.postgresql.Driver",
    "jdbc:postgresql://localhost/student?user=postgres&password=secret",
    "/mnt/app/play_test/play-test/app",
    "models", None, None, true, false
  )
}
