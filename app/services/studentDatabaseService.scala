package services

import models.Student
import models.Tables._
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}
class studentDatabaseService(db: Database)(implicit ec: ExecutionContext) {

  // testing DB
  def testDB(): Future[Boolean]= {
    println("getting DB info")
    val matches = db.run(Stud.filter(studRow => studRow.name === "ddorel" ).result)

    matches.map(studRows => studRows.nonEmpty)

  }

  // retrieve students from DB
  def getAllStudent(size: Int) = {
    db.run(Stud.take(size).result)
      .map(items => {
        //items: Vector[StudRow]
        items.map(item => Student(item.id, item.name, item.age, item.email))
      })
  }

  // insert student in DB and return id
  def insertStudent(username: String, age: Int, email: String) = {
    db.run(Stud returning Stud.map(_.id) += StudRow(username, age, email, -1))
  }

  // insert student in DB and return true if DB returns 1, else false
  def createStudent(username: String, age: Int, email: String) = {
    db.run(Stud += StudRow(username, age, email, -1)).map(_ > 0)
  }

  // update student by ID
  def updateStudentById(id: Int, name: String, age: Int, email: String) = {
    val query = for {
      stud <- Stud if (stud.id === id)
    } yield (stud.name, stud.age, stud.email)
    val action = query.update(name, age, email)
    db.run(action).map(_ > 0)
  }

  def getStudentById(id: Int) = {
    db.run (
      (for {
        stud <- Stud if(stud.id === id)
      } yield {
        stud
      }).result)
      .map(items => {
        println("We have items %s".format(items))
        items.map(item => {
          println("We have item %s".format(item))
          Student(item.id, item.name, item.age, item.email)
        } // item is StudRow
        )
      })
//    db.run(Stud.filter(_.id === id).result).map(items => {
//      println("We have items %s".format(items))
//      items.map(item => {
//        println("We have item %s".format(item))
//        Student(item.id, item.name, item.age, item.email)
//      } // item is StudRow
//      )
//    })
  }

  def deleteStudentById(id: Int) = {
    db.run(Stud.filter(_.id === id).delete).map(count => count > 0)
  }

  def getStudentByName(username: String): Future[String] = {
//    db.run(
//      (for {
//        user <- Stud if user.name == username
//      } yield {
//        Option(user.name)
//      }).result.head
//    )
    ???
  }

  def removeStudentById(id: Int) = ???


}
