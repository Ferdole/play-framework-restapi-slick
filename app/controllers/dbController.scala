package controllers

import services.studentDatabaseService
import javax.inject._
import play.api.mvc._
import play.i18n._
import play.api.libs.json._
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class dbController @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, cc: ControllerComponents)
                             (implicit ec: ExecutionContext) extends AbstractController(cc)
  with HasDatabaseConfigProvider[JdbcProfile]{

  protected val studentDBService = new studentDatabaseService(db)

//  implicit val studentReads: Reads[Student] = Json.reads[Student]
//  implicit val studentWrites: Writes[Student] = Json.writes[Student]

  // test DB
  def load() = Action.async { implicit request =>

    val test1 = studentDBService.testDB()

    test1.map { userExists =>
      println(userExists.toString)
      if (userExists) {
        Ok("Dorel in DB")
      } else {
        Ok("Dorel doesnt Exists")
      }
    }
  }

  /**
   *
   * @param size: default = 20. How many students to return from DB
   * @return List of students
   */
  def getAllStudent(size: Int = 20) = Action.async { implicit request =>
    studentDBService.getAllStudent(size).map (item =>
      Ok(Json.toJson(item))
    )
  }

  /**
   * If there are more keys than necessary to initiate case class StudentNoId an error will be thrown
   *
   * @return ID o inserted student
   */
  def insertStudent() = Action(parse.json).async { implicit request =>

    println("Received: %s".format(request.body))

    val validStudentNoId = request.body.as[StudentNoId]

    validStudentNoId match {
      case StudentNoId(name, age, email) => {
        println("We got valid Student")
        val insertedId = studentDBService.insertStudent(name, age, email)
        insertedId.map {
          // anonymous pattern matching
          case userId => Ok("Insered student with ID: %s".format(userId))
          case _ => Ok("Error")
        }
      }
    }
  }

  /**
   * Update student by ID. Body must be valid student
   *
   * @param id: Student's id
   * @return
   */

  def updateStudentById(id: Int) = Action(parse.json).async {implicit request =>
    println("Received: %s".format(request.body))
    val maps = request.body
    val studAs = maps.as[StudentNoId]

    studentDBService.updateStudentById(id, studAs.name, studAs.age, studAs.email).map(update =>
      if (update) Ok("Updated ID %d ...".format(id))
      else NotFound("ID not Found")
    )
  }

  /**
   * Get student by ID
   *
   * @param id
   * @return Json of Student if found, else Code 404
   */
  // return student by id
  def getStudentById(id: Int) = Action.async { implicit result =>
    studentDBService.getStudentById(id).map {
      case Vector(item: Student) => Ok(Json.toJson(item))
      case Nil => NotFound("Id not found")
    }
  }

  /**
   * Delete student by ID
   *
   * @param id
   * @return if found Confirmation of deleted ID, else Code 404
   */
  def deleteStudentById(id: Int) = Action.async { implicit request =>
    val deleted = studentDBService.deleteStudentById(id)
    deleted.map ( deleteCount => {
      if (deleteCount) Ok("Deleted student at ID: %d".format(id))
      else NotFound("Id not found")
    }
    )
  }

  /**
   * Create username w/o body just from the url. /createStudent/(name)/(age)/(email)
   *
   * @param username
   * @param age
   * @param email
   * @return
   */
  def createStudent(username: String, age: Int, email: String) = Action.async { implicit request =>
    studentDBService.createStudent(username, age, email).map { createStud =>
      if (createStud) {
        Ok("Created %s".format(username))
      } else {
        Ok(s"could not create $username")
      }
    }
  }

  def getStudentByName(username: String) = ???
}
