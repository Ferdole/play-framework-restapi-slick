package controllers

import models.Student

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def ping() = Action { implicit request =>
    Ok("This works")
  }

  def jjson() = Action { implicit request =>
    Ok(Json.obj("name" -> "hello", "bool" -> true))
  }

  def attr(name: String, lastName: String) = Action { _ =>
    Ok("name is %s and lastName %s and digi %d".format(name, lastName,3))
  }

  def postJson() = Action(parse.json) { implicit request =>
    println("String: %s ".format(request.body.toString()))
    Ok(Json.obj("received" -> Json.toJson(request.body)))
  }

  def studentListTest() = Action { implicit request =>
    val jsStud = Student(1 ,"Dorel", 12, "as@g.com")
    val jsStud1 = Student(1 ,"Haza", 12, "as@g.com")

    println("jsStud: " + jsStud)
    val seqStud = Seq(jsStud, jsStud1)
    Ok(Json.toJson(seqStud))
  }

}
