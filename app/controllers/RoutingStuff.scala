package controllers

import javax.inject._
import play.api.mvc.{BaseController, ControllerComponents}

class RoutingStuff @Inject() (val controllerComponents: ControllerComponents)  extends BaseController {

  def product(prodType: String, prodNum: Int) = Action {
    Ok(s"Product type is: $prodType, product number is: $prodNum")
  }

}
