name := """play-test"""
organization := "com.test"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.2"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "com.typesafe.play" %% "play-slick" % "5.0.0"
libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.3.2"

libraryDependencies ++= Seq(
  jdbc,
  "org.postgresql" % "postgresql" % "42.2.12"
//  "com.lightbend.slick" % "play-slick" % "4.0.0",
//  "com.lightbend.slick" % "play-json" % "2.8.1",
//  "com.lightbend.slick" % "slick-hikaricp" % "3.3.2"
)

// for testing database connection
libraryDependencies += jdbc % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.test.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.test.binders._"
